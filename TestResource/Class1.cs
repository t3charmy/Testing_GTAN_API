﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using GTANetworkServer;
using GTANetworkShared;

namespace TestResource
{
    public class NonGenericScriptName : Script
    {
        static NetHandle[] whales;
        static dynamic bans;
        static int clientID = 0;
        public NonGenericScriptName ()
        {
            API.onResourceStart += API_onResourceStart;
            API.onPlayerConnected += API_onPlayerConnected;
            API.onPlayerBeginConnect += API_onPlayerConnecting;
        }

        private void API_onResourceStart ()
        {
            bans = API.fromJson(File.ReadAllText("bans.json"));
            Console.WriteLine(bans.Entries);
        }

        private void API_onPlayerConnected (Client player)
        {
            API.sendChatMessageToPlayer(player, "Yes.");
            clientID += 1;
            API.setEntityData(player, "ClientID", clientID);

        }

        private void API_onPlayerConnecting(Client player, CancelEventArgs e)
        {
            string userIP = API.getPlayerAddress(player);
            if (bans.Entries.Contains(userIP) == true);
            {
                // e.Reason = "Banned";
                // e.Cancel = true;
                //API.kickPlayer(player, "You're IP banned from this server.");
                Console.WriteLine(player.name + " attempted to join but is banned");
            }
        }

        [Command("invcarset", GreedyArg = true)]
        public void invcarset(Client sender, string key, string value)
        {
            NetHandle playerCar = API.getPlayerVehicle(sender);
            API.setEntityData(playerCar, key, value);
        }

        [Command("invcarget")]
        public void invcarget(Client sender, string key)
        {
            NetHandle playerCar = API.getPlayerVehicle(sender);
            string value = API.getEntityData(playerCar, key);
            API.sendNotificationToPlayer(sender, "Returned Value: " + value);
        }

        [Command("kindofawhale")]
        public void kindofawhale(Client sender)
        {
            Vector3 WhaleLoc = new Vector3(0, 0, 0);
            int uID = API.getEntityData(sender, "ClientID");
            whales[uID] = API.createPed(API.pedNameToModel("KillerWhale"), WhaleLoc, 0);
            //Vector3 playerLoc = API.getEntityPosition(sender);
            //Vector3 playerHead = API.getEntityRotation(sender);
            //API.setEntityPosition(whales[uID], playerLoc);
            //API.setEntityRotation(whales[uID], playerHead);
            API.attachEntityToEntity(whales[uID], sender, "SKEL_Head", new Vector3(), new Vector3());
        }

    }
}
